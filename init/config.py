import environs
import os


basedir = os.path.abspath(os.path.dirname(__file__))
env = environs.Env()
env.read_env()
bot_token = env("BOT_TOKEN")
external_ip = env("EXTERNAL_IP")


class Config(object):
    DEBUG = True
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = env("SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

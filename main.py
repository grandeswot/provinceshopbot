import time
import telebot
from flask import request
from telebot import types

from init import application, bot_token, external_ip

bot = telebot.TeleBot(bot_token, threaded=False)
bot.remove_webhook()
time.sleep(1)
bot.set_webhook(url="https://provinceshopbot.ru/")


def web_app_keyboard():
    keyboard = types.ReplyKeyboardMarkup(row_width=1) #создаем клавиатуру
    web_app_test = types.WebAppInfo("https://provinceshopbot.ru/catalog/") #создаем webappinfo - формат хранения url
    one_butt = types.KeyboardButton(text="Тестовая страница", web_app=web_app_test) #создаем кнопку типа webapp
    keyboard.add(one_butt) #добавляем кнопки в клавиатуру

    return keyboard


@application.route('/catalog')
def catalog():
    return "<h1 style='color:blue'>Я Каталог - я буду скоро здесЯ!</h1>"


@application.route('/', methods=["POST"])
def webhook():
    bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
    return "ok", 200

# @application.route("/")
# def hello():
#     return "<h1 style='color:blue'>Hello There!</h1>"


@bot.message_handler(commands=['start', 'help'])
def start_command(message):
    bot.send_message(message.chat.id, f"Уважаемый(ая) {message.chat.first_name} {message.chat.first_name}, добро пожаловать в Оттенки Сладкого", parse_mode='Markdown', reply_markup=web_app_keyboard())


if __name__ == "__main__":
    application.run(host="0.0.0.0")

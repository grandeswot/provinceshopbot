from init import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, unique=True)
    username = db.Column(db.String)
    addresses = db.relationship("Address", backref="user")
    orders = db.relationship("Order", backref="user")

    def __repr__(self):
        return f"{self.id}, {self.username}"


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        return f"{self.id}, {self.address}"


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.String)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    products = db.relationship("Product", backref="order")


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    image_path = db.Column(db.String)
    price = db.Column(db.DECIMAL)
    order_id = db.Column(db.Integer, db.ForeignKey("order.id"))
    categories = db.relationship("Category", secondary="ProductCategoryLnk", backref="products")


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    products = db.relationship("Product", secondary="ProductCategoryLnk", backref="categories")


class ProductCategoryLnk(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey("category.id"))
    product_id = db.Column(db.Integer, db.ForeignKey("product.id"))
